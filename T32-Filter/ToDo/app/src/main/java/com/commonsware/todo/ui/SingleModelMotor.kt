package com.commonsware.todo.ui

import androidx.lifecycle.*
import com.commonsware.todo.repo.ToDoModel
import com.commonsware.todo.repo.ToDoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class SingleModelViewState(
  val item: ToDoModel? = null
)

class SingleModelMotor(
  private val repo: ToDoRepository,
  modelId: String?
) : ViewModel() {
  val states: LiveData<SingleModelViewState> =
    modelId?.let { repo.find(modelId).map { SingleModelViewState(it) }.asLiveData() }
      ?: MutableLiveData<SingleModelViewState>(SingleModelViewState(null))

  fun save(model: ToDoModel) {
    viewModelScope.launch(Dispatchers.Main) {
      repo.save(model)
    }
  }

  fun delete(model: ToDoModel) {
    viewModelScope.launch(Dispatchers.Main) {
      repo.delete(model)
    }
  }
}