package com.commonsware.todo.repo

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ToDoRepository(private val store: ToDoEntity.Store) {
  fun items(): Flow<List<ToDoModel>> =
    store.all().map { all -> all.map { it.toModel() } }

  fun find(id: String): Flow<ToDoModel?> = store.find(id).map { it?.toModel() }

  suspend fun save(model: ToDoModel) {
    store.save(ToDoEntity(model))
  }

  suspend fun delete(model: ToDoModel) {
    store.delete(ToDoEntity(model))
  }
}